QT += core network gui widgets sql

CONFIG += c++11

TARGET = NotesApp

CONFIG += console
CONFIG -= app_bundle
DESTDIR = ../../bin

TEMPLATE = app

SOURCES += main.cpp



INCLUDEPATH += ../NotesLib
LIBS += -L../../lib -lNotesLib
