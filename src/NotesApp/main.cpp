#include <QApplication>
#include <QtCore>
#include "presenternotes.h"
#include <memory>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    std::unique_ptr<PresenterNotes> presenter(new PresenterNotes(nullptr));

    return a.exec();
}
