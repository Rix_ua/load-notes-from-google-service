#pragma once
#include "stdafx.h"
#include "Enums.h"
namespace Parse
{
    class ParserHtml
    {

    public:

        ParserHtml();

    public:

        const QString GetCookies(const QString &, const  QString & key)const;
        const QString GetGAPS(const QString&)const;
        const QString GetProfile(const QString &, const QString &)const;
        const QString GetLinkImage(const QString&);
    private:
        const QString GetValue(const QString &, const int & indexStart)const;

    };

}
