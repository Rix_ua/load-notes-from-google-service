#pragma once
#include "stdafx.h"
#include "NetworkManager.h"
namespace WorkingInNetwork
{

    class DownloadManager :
        public NetworkManager
    {
        Q_OBJECT
    public:
        explicit DownloadManager(QVector<Structs::Note>* notes,QObject *parent = 0);
        ~DownloadManager();

    public:
        void SetToken(const QString&)throw();
        void GetLinkForImage(const QString&, const QString&);
        void GetImage(const QString&, const QString& = "jpeg", bool mediaOrAnnotation = false);
        void SetPathSaveImage(const QString&)throw();
        void LoadMedia();

    signals:
        void SignalTextNotesReady();
        void SignalWorkingDataBase();
        void SignalLoadCompleted();
        void SignalMessageStatusBar(const QString&);

    public slots:
         virtual void SlotGetNotes();

    private slots:
         virtual void SlotReadyRead(QNetworkReply*);
         virtual void SlotGetLink(QNetworkReply*);
         virtual void SlotWriteImage(QNetworkReply*);

    private:
        void WritePathForImage(const QString&, bool mediaOrAnnotation=false)throw();

    private:
        QVector<Structs::Note> *m_notes;
        QEventLoop m_firstLoop;
        QString m_pathSaveImage;
        QString m_pathWhereSaveImage;
        QString m_type;
    };

}
