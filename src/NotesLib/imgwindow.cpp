#include "stdafx.h"
#include "imgwindow.h"

ImgWindow::ImgWindow(QWidget *parent, const QString& filePath, const QString& info)
    : QWidget(parent)
{
    setWindowFlags(Qt::Window);
    m_layout = new QVBoxLayout;
    m_layout->addWidget(&m_imgLabel);
    setLayout(m_layout);
    setWindowTitle(info);
    m_imgLabel.setPixmap(QPixmap(filePath));
    m_imgLabel.setAlignment(Qt::AlignCenter);
    m_imgLabel.setMinimumSize(10, 10);
}
