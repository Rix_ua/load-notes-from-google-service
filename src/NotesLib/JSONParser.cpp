#include "stdafx.h"
#include "JSONParser.h"

namespace Parse
{

    JSONParser::JSONParser()
    {
    }
    // method for receiving tokens from the document Json
    const QString JSONParser::GetValue(const QString& JsStr, const QString& key)
    {
        int indexStart = JsStr.indexOf(":", (JsStr.indexOf(key))) + EParse::EParseJson;
        int indexEnd = (JsStr.indexOf("\"", indexStart));

        return (JsStr.mid(indexStart, indexEnd - indexStart));
    }


    // Parsing json file
    void JSONParser::ParsJson(const QJsonObject& obj, QVector<Structs::Note>& vec)
    {
        QJsonArray jsonArray = obj["nodes"].toArray();


        foreach (const QJsonValue & value, jsonArray)
        {
            QJsonObject obj = value.toObject();
            if (obj.value("parentId").toString() == "root" )
            {
                Structs::Note newNote;
                newNote.parentId = obj.value("id").toString();
                newNote.isArchived = obj.value("isArchived").toBool();
                if (obj["annotationsGroup"].toObject()["annotations"].isArray())
                {
                    GetAnnotation(obj, newNote);
                }
                // search of the enclosed notes across the field of parentID
                foreach(const QJsonValue & value2, jsonArray)
                {
                    QJsonObject obj2 = value2.toObject();
                    QString parId = obj2.value("parentId").toString();

                    if (newNote.parentId == parId)
                    {
                        // Text note
                        if (!obj2.value("text").toString().isEmpty())
                            newNote.LIST_Notes.push_back(obj2.value("text").toString());


                        if (!obj2["blob"].toObject().value("media_id").toString().isEmpty())
                        {
                            GetMedia_Id(obj2, newNote);
                        }
                        // useful data on a note
                        newNote.created = obj2["timestamps"].toObject().value("created").toString();
                        newNote.updated = obj2["timestamps"].toObject().value("updated").toString();
                        newNote.deleted = obj2["timestamps"].toObject().value("deleted").toString();
                        newNote.trashed = obj2["timestamps"].toObject().value("trashed").toString();

                    }
                }


                CleaningField(newNote.created);
                CleaningField(newNote.updated);
                CleaningField(newNote.deleted);
                CleaningField(newNote.trashed);

                // we lay down a ready note in a vector
                vec.push_back(newNote);
            }
        }
    }

    // Take  field for image
    void JSONParser::GetMedia_Id(const QJsonObject& blob, Structs::Note& note)
    {
        Structs::Media media;
        media.media_id = blob["blob"].toObject().value("media_id").toString();
        media.type = blob["blob"].toObject().value("mimetype").toString();
        note.Images.push_back(media);
    }

    // Parsing field AnnotationGroup
    void JSONParser::GetAnnotation(const QJsonObject& obj, Structs::Note& note)
    {
        QJsonArray arr = obj["annotationsGroup"].toObject()["annotations"].toArray();
        Structs::Annotations ann;
        foreach (const QJsonValue& value , arr)
        {
            QJsonObject obj2 = value.toObject();

            ann.Url = obj2.value("webLink").toObject().value("url").toString();
            ann.title = obj2.value("webLink").toObject().value("title").toString();
            ann.provenanceUrl = obj2.value("webLink").toObject().value("provenanceUrl").toString();
            ann.imageUrl = obj2.value("webLink").toObject().value("imageUrl").toString();
            note.AnnNotes.push_back(ann);
        }
    }

    void JSONParser::CleaningField(QString& strChange)
    {
        if(strChange.indexOf("00:00:00") != ECheckErrorLogin::EError || strChange.isEmpty())
        {
            strChange.clear();
            strChange = "-";
            return;
        }
        // we cut off the last values of a line
        strChange = strChange.mid(0, strChange.lastIndexOf('.'));
    }
}
