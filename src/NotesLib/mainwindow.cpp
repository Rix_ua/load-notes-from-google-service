#include "stdafx.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "treeitem.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setCentralWidget(ui->stackedWidget);
    setWindowTitle("Download Google Keep Notes");

    QVBoxLayout *layout = new QVBoxLayout();
    layout->addWidget(ui->treeView);
    ui->page_Notes->setLayout(layout);

//    QVBoxLayout *layout2 = new QVBoxLayout();
//    layout2->addWidget(ui->labelPleaseWait, 0, Qt::AlignCenter);
//    ui->page_ProggresBar->setLayout(layout2);

//    QVBoxLayout *layout0 = new QVBoxLayout();
//    layout0->addWidget(ui->verticalLayoutWidget, 0, Qt::AlignCenter);
//    ui->lineEdit->setFixedWidth(200);
//    ui->page_Login->setLayout(layout0);

    SetDefaultState();

    connect(ui->signIn, &QPushButton::clicked, this, &MainWindow::SlotSignInClicked );
}

void MainWindow::SlotSignInClicked()
{
    QPair<QString, QString> login;
    login.first = ui->lineEdit->text();
    login.second = ui->lineEdit_2->text();
    ui->stackedWidget->setCurrentWidget(ui->page_ProggresBar);
    ui->statusBar->showMessage("Connection ...");
    emit SignIn(login);
}

void MainWindow::SlotShowPageWithNotes(const QVector<Structs::Note>& notes)
{
    m_model.reset(new TreeModel(notes, nullptr));
    ui->treeView->setModel(m_model.get());

    for (int column = 0; column < m_model->columnCount(); column++)
        ui->treeView->resizeColumnToContents(column);

    ui->stackedWidget->setCurrentWidget(ui->page_Notes);

    m_pathOpenImgages = QCoreApplication::applicationDirPath() + "/DB/" + ui->lineEdit->text();

    m_selectionModel.reset( ui->treeView->selectionModel());
    connect(m_selectionModel.get(), &QItemSelectionModel::selectionChanged, this, &MainWindow::SlotSelectionChanged);

    resize(800, 400);
}

void MainWindow::SetDefaultState()
{
    ui->actionSign_out->setEnabled(false);
    ui->lineEdit->clear();
    ui->lineEdit_2->clear();
    ui->stackedWidget->setCurrentWidget(ui->page_Login);
    ui->signIn->setDefault(true);
    ui->signIn->setAutoDefault(true);
    m_isLoadCompleted = false;
    ui->statusBar->showMessage("Login Page");
}

void MainWindow::ShowErrorMessage(const QString &msgError)
{
    int ans = QMessageBox::warning(this, "WARNING", msgError, QMessageBox::Ok);
    if (ans == QMessageBox::Ok)
        this->SetDefaultState();
}

void MainWindow::SlotSelectionChanged(const QItemSelection &, const QItemSelection &)
{
    //get the text of the selected item
    const QModelIndex index = ui->treeView->selectionModel()->currentIndex();

    QString selectedText = index.data().toString();
    //find out the hierarchy level of the selected item
    int hierarchyLevel = 1;
    QModelIndex seekRoot = index;
    while(seekRoot.parent() != QModelIndex())
    {
        seekRoot = seekRoot.parent();
        hierarchyLevel++;
    }
    QString showString = QString("%1, Level %2").arg(selectedText)
                         .arg(hierarchyLevel);

    int col = index.column();
    TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
    if (true == m_isLoadCompleted && 0 == col &&
            (EImgLink == item->identifier() || EAnnImgLink == item->identifier()) )
    {
        QString fPath;
        if (EImgLink == item->identifier())
            fPath = m_pathOpenImgages + "/" + selectedText;
        else
        {
            QString partOfAnnotation = selectedText.mid(selectedText.lastIndexOf('/'), selectedText.length() - selectedText.lastIndexOf('/'));
            fPath = m_pathOpenImgages +  partOfAnnotation;
        }
        m_imgWnd.reset(new ImgWindow (this, fPath, showString) );
        m_imgWnd->show();
    }
}

void MainWindow::SlotMessageStatusBar(const QString& str)
{
    ui->statusBar->showMessage(str);
}

void MainWindow::SlotImagesCanShown()
{
    m_isLoadCompleted = true;
    ui->statusBar->showMessage("Loading images is completed. You can open them.");
    ui->actionSign_out->setEnabled(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionSign_out_triggered()
{
    m_selectionModel.reset();
    m_imgWnd.reset();
    m_model.reset();
    SetDefaultState();
}

void MainWindow::on_actionExit_triggered()
{
    close();
}
