/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionSign_out;
    QAction *actionExit;
    QWidget *centralWidget;
    QStackedWidget *stackedWidget;
    QWidget *page_Login;
    QPushButton *signIn;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_pwd;
    QLineEdit *lineEdit_2;
    QWidget *layoutWidget1;
    QVBoxLayout *verticalLayout;
    QLabel *label_email;
    QLineEdit *lineEdit;
    QWidget *page_ProggresBar;
    QLabel *label;
    QWidget *page_Notes;
    QTreeView *treeView;
    QMenuBar *menuBar;
    QMenu *menuMenu;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(400, 300);
        MainWindow->setMinimumSize(QSize(400, 300));
        actionSign_out = new QAction(MainWindow);
        actionSign_out->setObjectName(QStringLiteral("actionSign_out"));
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        stackedWidget->setGeometry(QRect(0, 0, 401, 241));
        page_Login = new QWidget();
        page_Login->setObjectName(QStringLiteral("page_Login"));
        signIn = new QPushButton(page_Login);
        signIn->setObjectName(QStringLiteral("signIn"));
        signIn->setGeometry(QRect(110, 190, 171, 23));
        layoutWidget = new QWidget(page_Login);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(110, 120, 171, 41));
        verticalLayout_2 = new QVBoxLayout(layoutWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        label_pwd = new QLabel(layoutWidget);
        label_pwd->setObjectName(QStringLiteral("label_pwd"));

        verticalLayout_2->addWidget(label_pwd);

        lineEdit_2 = new QLineEdit(layoutWidget);
        lineEdit_2->setObjectName(QStringLiteral("lineEdit_2"));
        lineEdit_2->setEchoMode(QLineEdit::Password);

        verticalLayout_2->addWidget(lineEdit_2);

        layoutWidget1 = new QWidget(page_Login);
        layoutWidget1->setObjectName(QStringLiteral("layoutWidget1"));
        layoutWidget1->setGeometry(QRect(110, 70, 171, 41));
        verticalLayout = new QVBoxLayout(layoutWidget1);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        label_email = new QLabel(layoutWidget1);
        label_email->setObjectName(QStringLiteral("label_email"));

        verticalLayout->addWidget(label_email);

        lineEdit = new QLineEdit(layoutWidget1);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        verticalLayout->addWidget(lineEdit);

        stackedWidget->addWidget(page_Login);
        page_ProggresBar = new QWidget();
        page_ProggresBar->setObjectName(QStringLiteral("page_ProggresBar"));
        label = new QLabel(page_ProggresBar);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(150, 110, 111, 61));
        stackedWidget->addWidget(page_ProggresBar);
        page_Notes = new QWidget();
        page_Notes->setObjectName(QStringLiteral("page_Notes"));
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(page_Notes->sizePolicy().hasHeightForWidth());
        page_Notes->setSizePolicy(sizePolicy);
        treeView = new QTreeView(page_Notes);
        treeView->setObjectName(QStringLiteral("treeView"));
        treeView->setGeometry(QRect(80, 30, 251, 191));
        stackedWidget->addWidget(page_Notes);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 400, 21));
        menuMenu = new QMenu(menuBar);
        menuMenu->setObjectName(QStringLiteral("menuMenu"));
        MainWindow->setMenuBar(menuBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        QWidget::setTabOrder(lineEdit, lineEdit_2);
        QWidget::setTabOrder(lineEdit_2, signIn);

        menuBar->addAction(menuMenu->menuAction());
        menuMenu->addAction(actionSign_out);
        menuMenu->addAction(actionExit);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        actionSign_out->setText(QApplication::translate("MainWindow", "Sign out", 0));
        actionExit->setText(QApplication::translate("MainWindow", "Exit", 0));
        signIn->setText(QApplication::translate("MainWindow", "Sign in", 0));
        label_pwd->setText(QApplication::translate("MainWindow", " Password", 0));
        label_email->setText(QApplication::translate("MainWindow", " Email (google.com)", 0));
        label->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:14pt; color:#666666;\">Please wait...</span></p></body></html>", 0));
        menuMenu->setTitle(QApplication::translate("MainWindow", "Menu", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
