#pragma once
#include "stdafx.h"
#include <condition_variable>
class SynhRequest
{
public:
	SynhRequest();
	~SynhRequest();

public:
    void Wait(int Time_Ms);
private:
	std::condition_variable cond_var;
	std::mutex Synk_Req;
};

