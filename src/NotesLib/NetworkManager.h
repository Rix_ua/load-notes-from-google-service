
#pragma once
#include "SettingManager.h"

namespace WorkingInNetwork
{


    class NetworkManager :public QObject
    {
        Q_OBJECT
    public:

        explicit NetworkManager(QObject * parent);
        virtual ~NetworkManager();

    signals:
        void GetToken();
        void SignalGetErrorStr(const QString &);
        void SignalStopLoop();

    protected slots:
        virtual void SlotGetFinished();
        virtual void SlotReplyPostFinished();
        virtual void SlotGetCookies();
        virtual void SlotGetCode();
        virtual void SlotPostGrantType(QNetworkReply*);
        virtual void SlotPostRefreshToken(QNetworkReply*);
        virtual void SlotPostIssueToken(QNetworkReply*);

    public:
        bool IsToken()const throw();
        QString GetTok()const throw();
        // the method causing the beginnings of turn of inquiries
        void QueueInquiries();
        void SetLogin(const QPair<QString, QString>& login);

    protected:
        void Init(void);

    private:
        void CheckError(const QString&, bool flag);
        void FisrtPageRequest();
        void SecondPageRequest(const QString &);
        void ThirdRequest(const QString&);
        void GetCode();
        void PostGrantType();
        void PostRefreshToken(const QString&);
        void PostIssueToken(const QString&);

    protected:
        std::shared_ptr<QNetworkAccessManager> m_networkManager;
        QNetworkReply * m_reply;
        QNetworkRequest m_request;
        QByteArray m_postData;
        QString m_replyStr;
        Structs::RequestField m_reqParams;
        QList<QNetworkCookie> m_cookies;
        QNetworkCookieJar * n_jar;
        QString m_code;
        QString m_token;
        SettingManager m_setting;
        QPair<QString, QString> m_login;
    };
}
