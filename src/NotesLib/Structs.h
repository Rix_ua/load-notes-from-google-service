#pragma once
#include "stdafx.h"
namespace Structs
{
    struct Media
    {
    public:
        QString type;
        QString media_id; // it is used in exile for downloading of the file
    };

    struct Annotations
    {
        QString Url;
        QString title;
        QString imageUrl;
        QString provenanceUrl;
    };
    struct Note
    {
    public:
        QString created;
        QString updated;
        QString trashed;
        QString deleted;
        bool isArchived;

        QString parentId;
        QVector<QString> LIST_Notes;
        QVector<Media> Images;
        QVector<Annotations> AnnNotes;
    };

    struct RequestField
    {
    public:
        // Undertake from the first file
        QString GALX;
        QString gxf;

        // this field gets from the second inquiry
        QString ProfileInformation;

    };

}
