#pragma once
#include "stdafx.h"
#include "Structs.h"
#include "Def.h"
#include <QTextStream>
namespace WorkingInNetwork
{

    class SettingManager
    {

    public:
        SettingManager();
        virtual ~SettingManager();

    public:
        void WriteRequestFirst(QNetworkRequest&)throw();
        void WriteRequestSecond(QNetworkRequest &, const QString&, Structs::RequestField &, QByteArray&)throw();
        void WriteRequestThrid(QNetworkRequest &, const QString&, const QString&, Structs::RequestField &, QByteArray&)throw();
        void WriteRequestGetCode(QNetworkRequest&)throw();
        void WriteRequestGrantType(QNetworkRequest&, const QString&, QByteArray&)throw();
        void WriteRequestRefreshToke(QNetworkRequest&, const QString&, QByteArray&)throw();
        void WriteRequestIssueToken(QNetworkRequest&, const QString&, QByteArray&, QByteArray&)throw();
        void WriteRequestGetNotes(QNetworkRequest&, QByteArray&,const QString & )throw();
        void WriteRequestGetLinkForImage(QNetworkRequest&, const QString&, const QByteArray&)throw();
        void WriteRequestForImage(QNetworkRequest&, const QByteArray&, const QString&)throw();
    private:
        void WriteHeadRequest(QNetworkRequest& request)throw();
        void WriteRawHeader(QNetworkRequest&)throw();
        void WriteHeadLinkRequest(QNetworkRequest&, const QByteArray&)throw();

    };
}
