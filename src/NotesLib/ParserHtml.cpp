#include "stdafx.h"
#include "ParserHtml.h"


namespace Parse
{
    ParserHtml::ParserHtml()
    {
    }


    const QString ParserHtml::GetCookies(const QString & s_Html, const  QString & key)const
    {
        int indexCookie = s_Html.indexOf(key);
        return (GetValue(s_Html, indexCookie));
    }


    // Get Field ProfileInformation, or keys
    const QString ParserHtml::GetProfile(const QString &s_Html, const QString &key)const
    {
        int indexStart = s_Html.indexOf(":", (s_Html.indexOf(key))) + EParse::ECookie;
        int indexEnd = (s_Html.indexOf("\"", indexStart));

        return (s_Html.mid(indexStart, indexEnd - indexStart));
    }


    const QString ParserHtml::GetGAPS(const QString&s_Html) const
    {
        int indexStart = (s_Html.indexOf("GAPS")+ EParse::EGaps);

        int indexEndValue = (s_Html.indexOf(";", indexStart));

        return (s_Html.mid(indexStart, indexEndValue));
    }
    // method for getting a link for downloading of the picture
    const QString ParserHtml::GetLinkImage(const QString& s_Html)
    {
        int indexStart = (s_Html.indexOf("HREF=\"") + EParse::EHREF);

        int indexEndValue = (s_Html.indexOf("\"", indexStart));

        return (s_Html.mid(indexStart, indexEndValue - indexStart));
    }
   // method for receiving kuk from the HTML page
    const QString ParserHtml::GetValue(const QString &s_Html, const int & indexStart)const
    {

        int indexValue = (s_Html.indexOf("=\"", indexStart) + EParse::ECookie);

        int indexValueEnd = (s_Html.indexOf("\"", indexValue));

        return (s_Html.mid(indexValue, indexValueEnd - indexValue));
    }



}

