
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPair>
#include <QString>
#include <QMessageBox>
#include <QVBoxLayout>
#include <QItemSelectionModel>
#include <memory>
#include "treemodel.h"
#include "imgwindow.h"
#include "Enums.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent);
    void ShowErrorMessage(const QString &msgError);
    ~MainWindow();

public slots:
    void SlotSignInClicked();
    void SlotShowPageWithNotes(const QVector<Structs::Note>& notes);
    void SlotSelectionChanged(const QItemSelection &, const QItemSelection &);
    void SlotMessageStatusBar(const QString&);
    void SlotImagesCanShown();

signals:
    void SignIn(const QPair<QString, QString>&);

private slots:
    void on_actionSign_out_triggered();

    void on_actionExit_triggered();

private:
    void SetDefaultState();
private:
    Ui::MainWindow *ui;
    std::unique_ptr<TreeModel> m_model;
    std::unique_ptr<ImgWindow> m_imgWnd;
    std::unique_ptr<QItemSelectionModel> m_selectionModel;
    QString m_pathOpenImgages;
    bool m_isLoadCompleted;
};

#endif // MAINWINDOW_H
