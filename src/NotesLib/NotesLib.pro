QT += core network gui widgets sql

CONFIG += c++11

TARGET = NotesLib

CONFIG += console
CONFIG -= app_bundle
DESTDIR = ../../lib

TEMPLATE = lib
CONFIG += staticlib
SOURCES += \
    JSONParser.cpp \
    NetworkManager.cpp \
    ParserHtml.cpp \
    stdafx.cpp \
    SettingManager.cpp \
    mainwindow.cpp \
    presenternotes.cpp \
    DownloadManager.cpp \
    treeitem.cpp \
    treemodel.cpp \
    DataBase.cpp \
    imgwindow.cpp

HEADERS += \
    Def.h \
    JSONParser.h \
    ParserHtml.h \
    NetworkManager.h \
    NetworkManager.h \
    stdafx.h \
    SettingManager.h \
    Structs.h \
    mainwindow.h \
    presenternotes.h \
    Enums.h \
    DownloadManager.h \
    treeitem.h \
    treemodel.h \
    DataBase.h \
    imgwindow.h


FORMS += \
    mainwindow.ui

PRECOMPILED_HEADER = stdafx.h
