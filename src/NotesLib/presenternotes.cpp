#include "stdafx.h"
#include "presenternotes.h"
#include "mainwindow.h"
#include "DownloadManager.h"
#include "DataBase.h"

using namespace WorkingInNetwork;
PresenterNotes::PresenterNotes(QObject *parent) : QObject(parent)
{
    m_view.reset(new MainWindow(nullptr));
    connect(m_view.get(), &MainWindow::SignIn, this, &PresenterNotes::signIn);
    connect(this, &PresenterNotes::signalTextNotesReady, m_view.get(), &MainWindow::SlotShowPageWithNotes);

    m_view->show();
}

void PresenterNotes::signIn(const QPair<QString, QString>& login)
{
    if (!m_notes.isEmpty())
        m_notes.clear();

    m_networkManager.reset(new NetworkManager(nullptr));
    connect(m_networkManager.get(), &NetworkManager::GetToken, this, &PresenterNotes::slotGetNotes);
    connect(m_networkManager.get(), &NetworkManager::SignalGetErrorStr, this, &PresenterNotes::SlotGetErrorStr);
    this->m_user_login = login.first;
    m_networkManager->SetLogin(login);
    m_networkManager->QueueInquiries();
}

void PresenterNotes::slotGetNotes()
{
    m_downloadManager.reset(new DownloadManager(&m_notes, nullptr));

    connect(m_downloadManager.get(), &DownloadManager::SignalTextNotesReady, this, &PresenterNotes::slotTextNotesReady);
    connect(m_downloadManager.get(), &DownloadManager::SignalWorkingDataBase, this, &PresenterNotes::writeDataBase);

    m_downloadManager->SetToken(m_networkManager->GetTok());
    m_downloadManager->SlotGetNotes();
}

void PresenterNotes::slotTextNotesReady ()
{
    emit signalTextNotesReady(m_notes);
}

void PresenterNotes::writeToken(const QString& tok )
{
    this->m_token = tok;
}

void PresenterNotes::writeDataBase()
{
    m_db.reset(new DataBase(this->m_user_login));
    m_db->InsetNote(this->m_notes);

    SlotLoadPictures();
}
void PresenterNotes::SlotGetErrorStr(const QString & msgError)
{
    m_view->ShowErrorMessage(msgError);
}

void PresenterNotes::SlotLoadPictures()
{
    m_downloadManager.reset(new DownloadManager(&m_notes, nullptr));
    connect(m_downloadManager.get(), &DownloadManager::SignalLoadCompleted, m_view.get(), &MainWindow::SlotImagesCanShown);
    connect(m_downloadManager.get(), &DownloadManager::SignalMessageStatusBar, m_view.get(), &MainWindow::SlotMessageStatusBar);

    m_downloadManager->SetPathSaveImage(this->m_user_login);
    m_downloadManager->SetToken(m_networkManager->GetTok());
    m_downloadManager->LoadMedia();
}

 PresenterNotes::~PresenterNotes()
 {
 }

