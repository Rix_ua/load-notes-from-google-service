#pragma once

class DataBase: public QObject
{
    Q_OBJECT
public:
    DataBase(const QString&);
    virtual ~DataBase();

public:
    QSqlError InsetNote(const QVector<Structs::Note>&);

private:
    void AddNote(QSqlQuery&, const QVector<QString>& , const QString&, const QString&, const QString&,const QString&,const QString&, const bool&);
    void AddImage(QSqlQuery&, const QVector<Structs::Media>&, const QString&);
    void AddAnnotations(QSqlQuery&, const QVector<Structs::Annotations>&, const QString&);
    void GetPath(const QString&, QString&);

private:

    QSqlDatabase m_db;
};

