#include "stdafx.h"
#include "DownloadManager.h"
#include "ParserHtml.h"
#include "Def.h"
#include "JSONParser.h"

using namespace Parse;
namespace WorkingInNetwork
{


    DownloadManager::DownloadManager(QVector<Structs::Note>* notes,
                                     QObject  * p):NetworkManager(p),m_notes(notes)
    {
    }


    DownloadManager::~DownloadManager()
    {
    }



    void DownloadManager::LoadMedia()
    {
        int coutLoadedImg = 0;
        for(int i = 0; i < this->m_notes->size();++i)
        {
            if(!this->m_notes->at(i).Images.isEmpty())
            {
                for (int j = 0; j < m_notes->at(i).Images.size(); ++j)
                {
                    GetLinkForImage(m_notes->at(i).Images.at(j).media_id, m_notes->at(i).Images.at(j).type);
                    coutLoadedImg++;
                    emit SignalMessageStatusBar(QString::number(coutLoadedImg) + " image loaded ...");
                    qDebug() << "Numb of Img" <<coutLoadedImg;
                }
            }

            if(!m_notes->at(i).AnnNotes.isEmpty())
            {
                for (int j = 0; j < m_notes->at(i).AnnNotes.size(); ++j)
                {
                    GetImage(m_notes->at(i).AnnNotes.at(j).imageUrl, "png", true);
                    coutLoadedImg++;
                    emit SignalMessageStatusBar(QString::number(coutLoadedImg) + " image loaded ...");
                    qDebug() << "Numb of Img" <<coutLoadedImg;
                }
            }
         }
        qDebug() <<"FInishLoad";
        emit SignalLoadCompleted();
    }

        // Request for receiving Json of the file with all a note
        void DownloadManager::SlotGetNotes()
        {
            QNetworkRequest request;
            Init();
            m_setting.WriteRequestGetNotes(request, m_postData, m_token);

            m_reply = m_networkManager->post(request, NetworkConst::LAST_REQ);


            QObject::connect(m_networkManager.get(), &QNetworkAccessManager::finished,
                this, &DownloadManager::SlotReadyRead);

        }
        // Parsing of the Json file with all notes
        void DownloadManager::SlotReadyRead(QNetworkReply*)
        {
            try
            {
                #ifdef _DEBUG
                    assert(m_reply);
                #endif // _DEBUG
                qDebug() <<"slotReadyRead";
                if (m_reply->error() == QNetworkReply::NoError)
                    this->m_replyStr = QString::fromUtf8(m_reply->readAll());
                else
                {
                    const std::string str = m_reply->errorString().toStdString();
                    throw std::exception(str.c_str());
                }
                Parse::JSONParser parse;
                QJsonDocument jsonDoc = QJsonDocument::fromJson(this->m_replyStr.toUtf8());


                if (jsonDoc.isObject())
                    parse.ParsJson(jsonDoc.object(), *m_notes);
                m_reply->deleteLater();
                std::cout << "Finish" << std::endl;
                #ifdef _DEBUG
                    assert(m_reply);
                #endif // _DEBUG
                emit SignalTextNotesReady();
                emit SignalWorkingDataBase();


                //networkManager.reset();
            }
            catch(std::exception& ex)
            {
                QString error(ex.what());
                emit SignalGetErrorStr(error);
            }

        }


        void DownloadManager::GetLinkForImage(const QString& media_id, const QString& typeImage)
        {
            QNetworkRequest request;
            Init();

            m_postData.clear();
            m_postData.append("Bearer " + m_token);

            m_setting.WriteRequestGetLinkForImage(request, media_id, m_postData);
            m_type = typeImage.mid(typeImage.lastIndexOf('/') +  EParse::EChangeType, typeImage.length() - typeImage.lastIndexOf('/') +  EParse::EChangeType);
            WritePathForImage(media_id);

            m_reply = m_networkManager->get(request);

            QObject::connect(m_networkManager.get(), &QNetworkAccessManager::finished,
               this, &DownloadManager::SlotGetLink);
            QObject::connect(this, &DownloadManager::SignalStopLoop, &m_firstLoop, &QEventLoop::quit);
            this->m_firstLoop.exec();

        }

        void DownloadManager::SlotGetLink(QNetworkReply *)
        {
            try
            {
                Parse::ParserHtml parse;
                #ifdef _DEBUG
                    assert(m_reply);
                #endif // _DEBUG
                if (this->m_reply->error() == QNetworkReply::NoError )
                    this->m_replyStr = QString::fromUtf8(m_reply->readAll());
                else
                {
                    const char *error = m_reply->errorString().toStdString().c_str();
                    throw std::exception(error);
                }
                m_reply->deleteLater();
                GetImage((parse.GetLinkImage(this->m_replyStr)), m_type);
            }
            catch(std::exception& ex)
            {
                emit SignalStopLoop();
                QString error(ex.what());
                emit SignalGetErrorStr(error);
            }
        }
        // method in which a request for the picture is sent
         void DownloadManager::GetImage(const QString& link, const QString& typeImage, bool mediaOrAnnotation)
         {
             QNetworkRequest request;
             if(mediaOrAnnotation)
             {
                 m_type = typeImage;
                 WritePathForImage(link,mediaOrAnnotation);
             }

             Init();
             request.setUrl(link);
             // Getting the line Host Link for inquiry
            QString hostLink = link.mid(EParse::ELengthHttps, EParse::ELenghtHostLinkForImage);

            m_setting.WriteRequestForImage(request, m_postData, hostLink);

            #ifdef _DEBUG
                assert(m_reply);
            #endif // _DEBUG
            m_reply = m_networkManager->get(request);

            QObject::connect(m_networkManager.get(), &QNetworkAccessManager::finished,
                             this, &DownloadManager::SlotWriteImage);
            if(mediaOrAnnotation)
            {
                QObject::connect(this, &DownloadManager::SignalStopLoop, &m_firstLoop, &QEventLoop::quit);
                m_firstLoop.exec();
            }
         }

         void DownloadManager::SlotWriteImage(QNetworkReply*)
         {
            try
             {
                 if(this->m_reply->error()  == QNetworkReply::NoError)
                 {
                     QFile newImage(this->m_pathWhereSaveImage);

                     QByteArray content = this->m_reply->readAll();

                     if(!newImage.open(QFile::WriteOnly))
                         qDebug() << "error open file";

                     newImage.write(content);
                    // newImage.close();

                 }
                 else
                 {
                     const std::string str = m_reply->errorString().toStdString();
                     throw std::exception(str.c_str());
                 }
                 qDebug() <<"Load Image";
                 emit SignalStopLoop();
             }
             catch(std::exception& ex)
             {
                 QString error(ex.what());
                 emit SignalGetErrorStr(error);
                 emit SignalStopLoop();
             }

         }

        void DownloadManager::SetToken(const QString & token)
        {
            this->m_token = token;
        }


        void DownloadManager::SetPathSaveImage(const QString& login)
        {
            QString path;
            QTextStream writStr(&path);
            QDir dir;

            writStr << QCoreApplication::applicationDirPath() << "/DB/" << login;
            // Creation of a directory for storage of pictures
            if(!dir.mkdir(path))
                qDebug() << "error create dir";

             this->m_pathSaveImage = path;
        }

        // method for receiving a way to the folder with pictures
        void DownloadManager::WritePathForImage(const QString& media, bool mediaOrAnnotation)
        {
            QTextStream writePath(&m_pathWhereSaveImage);
            this->m_pathWhereSaveImage.clear();
            if(!mediaOrAnnotation)
            {
                writePath << this->m_pathSaveImage << "/" << media << "." << m_type;
            }
            else
            {
                QString partOfAnnotation = media.mid(media.lastIndexOf('/'), media.length() - media.lastIndexOf('/'));

                writePath << this->m_pathSaveImage << "/" << partOfAnnotation << "." << m_type;
            }

        }
}
