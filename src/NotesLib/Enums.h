#pragma once

enum EParse
{
    EChangeType = 1,
	ECookie = 2,
	EParseJson = 3,
	EGaps = 5,
    EHREF = 6,
    ELengthHttps = 8,
    ELenghtHostLinkForImage = 25
};

enum ECheckErrorLogin
{
    EError = -1,
    EPassword,
    EEmail
};

enum ETreeView
{
    EImgLink = 1,
    EAnnImgLink = 2,

    EThirdLevel = 3
};
