#pragma once
#include "Structs.h"
#include "Enums.h"
#include <qjsondocument.h>
#include <qjsonobject.h>
#include <qjsonarray.h>
#include <qjsonvalue.h>
namespace Parse
{
    class JSONParser
    {
    public:
        JSONParser();

    public:
        const QString GetValue(const QString &, const QString &);
        // parsing method JsonDoc
        void ParsJson(const QJsonObject&, QVector<Structs::Note> &);

    private:
        void GetMedia_Id(const QJsonObject&, Structs::Note & );
        void GetAnnotation(const QJsonObject&, Structs::Note &);
        void CleaningField(QString&);
    };

}
