#ifndef PRESENTORNETES_H
#define PRESENTORNETES_H

#include "Structs.h"

namespace WorkingInNetwork
{
class NetworkManager;
class DownloadManager;

}
class DataBase;
class MainWindow;

class PresenterNotes : public QObject
{
    Q_OBJECT
public:
    explicit PresenterNotes(QObject *parent);
    virtual ~PresenterNotes();

signals:
    void signalTextNotesReady(const QVector<Structs::Note>& notes);
    void SignalLoadImage();
    void stopThread();

private slots:
    void signIn(const QPair<QString, QString>&);
    void slotTextNotesReady ();
    void slotGetNotes();
    void writeToken(const QString& );
    void writeDataBase();
    void SlotGetErrorStr(const QString& );
    void SlotLoadPictures();

private:
    std::unique_ptr<MainWindow> m_view;
    std::unique_ptr<QThread> m_thread;
    std::unique_ptr<WorkingInNetwork::NetworkManager> m_networkManager;
    std::unique_ptr<WorkingInNetwork::DownloadManager> m_downloadManager;
    std::unique_ptr<DataBase> m_db;
    QVector<Structs::Note> m_notes;
    QString m_token;
    QString m_user_login;
};

#endif // PRESENTORNETES_H
