#ifndef IMGWINDOW_H
#define IMGWINDOW_H

#include <QDialog>
#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>

class ImgWindow : public QWidget
{
    Q_OBJECT
public:
    ImgWindow(QWidget *parent, const QString& filePath, const QString& info);

private:
    QVBoxLayout * m_layout;
    QLabel m_imgLabel;
};

#endif // IMGWINDOW_H


