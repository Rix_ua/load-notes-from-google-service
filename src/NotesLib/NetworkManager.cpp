#include "stdafx.h"
#include "NetworkManager.h"
#include "ParserHtml.h"
#include "Def.h"
#include "JSONParser.h"

using namespace Parse;
namespace WorkingInNetwork
{



    NetworkManager::NetworkManager(QObject * parent):
        QObject(parent)
    {
        this->Init();
    }

    NetworkManager::~NetworkManager()
    {
    }

    void NetworkManager::Init(void)
    {
        this->m_replyStr.clear();
        this->m_networkManager.reset(new QNetworkAccessManager(this));
        this->n_jar = new QNetworkCookieJar();
        this->m_networkManager->setCookieJar(n_jar);

    }

    void NetworkManager::QueueInquiries()
    {
        this->FisrtPageRequest();
    }



    void NetworkManager::SetLogin(const QPair<QString, QString>& login)
    {
         m_login = login;
    }

    void NetworkManager::CheckError(const QString &strError, bool flag)
    {
        if(flag)
        {
            if(strError.indexOf("error_msg") != ECheckErrorLogin::EError)
            {
                throw std::exception("Unknown email");
            }
        }
        else
        {
            if(strError.indexOf("error-msg") != ECheckErrorLogin::EError)
            {
                throw std::exception("Incorrect password");
            }

        }
    }


    // method for a first request
    void NetworkManager::FisrtPageRequest()
    {
        Init();

        this->m_setting.WriteRequestFirst(this->m_request);

        this->m_reply = this->m_networkManager->get(this->m_request);

        QObject::connect(m_reply, &QNetworkReply::finished,
            this, &NetworkManager::SlotGetFinished);

    }

    void NetworkManager::SecondPageRequest(const QString & m_mail)
    {

        Init();
        if (this->m_cookies.count() != 0)
            this->n_jar->setCookiesFromUrl(this->m_cookies, NetworkConst::SecondUrl);

        this->m_setting.WriteRequestSecond(this->m_request, m_mail,this->m_reqParams,this->m_postData);

        this->m_reply = this->m_networkManager->post(this->m_request, this->m_postData);

        QObject::connect(m_reply, &QNetworkReply::finished,
            this, &NetworkManager::SlotReplyPostFinished);
//        this->syncObj.wait(2500);

    }



    // Method for receiving cookies
    void NetworkManager::ThirdRequest(const QString& pass)
    {
        Init();
        if (this->m_cookies.count() != 0)
            this->n_jar->setCookiesFromUrl(this->m_cookies, NetworkConst::ThirdUrl);


        this->m_setting.WriteRequestThrid(this->m_request, pass, m_login.first,this->m_reqParams, this->m_postData);

        this->m_reply = this->m_networkManager->post(this->m_request, this->m_postData);

        QObject::connect(m_reply, &QNetworkReply::finished,
            this, &NetworkManager::SlotGetCookies);

    }


    // Method for the fourth inquiry, on receiving code
    void NetworkManager::GetCode()
    {

        Init();
        this->n_jar->setCookiesFromUrl(this->m_cookies, NetworkConst::UrlGetCode);

        this->m_setting.WriteRequestGetCode(this->m_request);

        this->m_reply = this->m_networkManager->get(this->m_request);

        QObject::connect(m_reply, &QNetworkReply::finished,
            this, &NetworkManager::SlotGetCode);

    }

    /**
    * Следующие запросы будут на получение accessTokena
    * 1. запрос = grant_type
    * 2. запрос = refresh_token
    * 3. запрос = token
    * 4. JSON file
    */
    // request for receiving refresh_token
    void NetworkManager::PostGrantType()
    {
        Init();
        this->m_setting.WriteRequestGrantType(this->m_request, this->m_code, this->m_postData);

        this->m_reply = this->m_networkManager->post(this->m_request, this->m_postData);

        QObject::connect(this->m_networkManager.get(), &QNetworkAccessManager::finished,
            this, &NetworkManager::SlotPostGrantType);
    }

    // method for receiving access_token
    void NetworkManager::PostRefreshToken(const QString& Ref_Token)
    {
        Init();
        this->m_setting.WriteRequestRefreshToke(this->m_request, Ref_Token, this->m_postData);

        this->m_reply = this->m_networkManager->post(this->m_request, this->m_postData);

        QObject::connect(this->m_networkManager.get(), &QNetworkAccessManager::finished,
            this, &NetworkManager::SlotPostRefreshToken);
    }

    // method of request for token
    void NetworkManager::PostIssueToken(const QString& token)
    {
        Init();
        QByteArray arr;

        this->m_setting.WriteRequestIssueToken(this->m_request, token, this->m_postData, arr);

        this->m_reply = this->m_networkManager->post(this->m_request, arr);

        QObject::connect(this->m_networkManager.get(), &QNetworkAccessManager::finished,
             this, &NetworkManager::SlotPostIssueToken);

    }

    // Method of receiving information from the first answer
    void NetworkManager::SlotGetFinished()
    {
        try
        {

            this->m_networkManager->deleteResource(this->m_request);

            if(this->m_reply->error() == QNetworkReply::NoError)
            {
                Parse::ParserHtml parser;
                QString strReply = QString::fromUtf8(m_reply->readAll());

                this->m_reqParams.gxf = parser.GetCookies(strReply, "gxf");

                this->m_reqParams.GALX = parser.GetCookies(strReply, "GALX");

                this->m_cookies = this->n_jar->cookiesForUrl(NetworkConst::ThirdUrl);
    qDebug()<<"1";
                SecondPageRequest(m_login.first);
            }
            else
            {
                const std::string str = m_reply->errorString().toStdString();
                throw std::exception(str.c_str());
            }
        }
        catch (const std::exception &ex)
        {
            QString error(ex.what());
            emit SignalGetErrorStr(error);
        }
    }


    // Метод получения ответа из второго запроса
    void NetworkManager::SlotReplyPostFinished()
    {
        try
        {
            this->m_networkManager->deleteResource(this->m_request);
            if(this->m_reply->error() == QNetworkReply::NoError)
            {
                Parse::ParserHtml parser;
                QString strReply = QString::fromUtf8(m_reply->readAll());

                this->CheckError(strReply, ECheckErrorLogin::EEmail);

                this->m_reqParams.ProfileInformation = parser.GetProfile(strReply, "encoded_profile_information");

                this->m_cookies = qvariant_cast<QList<QNetworkCookie>>(m_reply->header(QNetworkRequest::SetCookieHeader));
                ThirdRequest(m_login.second);
            }
            else
            {
                const std::string str = m_reply->errorString().toStdString();
                throw std::exception(str.c_str());
            }
qDebug()<<"2";
        }
        catch (const std::exception &ex)
        {
            QString error(ex.what());
            emit SignalGetErrorStr(error);\
        }
    }

    void NetworkManager::SlotGetCookies()
    {
        try
        {
            this->m_networkManager->deleteResource(this->m_request);
            QString strReply = QString::fromUtf8(m_reply->readAll());
            if(this->m_reply->error() == QNetworkReply::NoError)
            {
                this->CheckError(strReply, ECheckErrorLogin::EPassword);

                this->m_cookies.clear();
                this->m_cookies = qvariant_cast<QList<QNetworkCookie>>(m_reply->header(QNetworkRequest::SetCookieHeader));

                GetCode();
            }
            else
            {
                const std::string str = m_reply->errorString().toStdString();
                throw std::exception(str.c_str());
            }
        }
        catch (const std::exception &ex)
        {
            QString error(ex.what());
            emit SignalGetErrorStr(error);
        }
qDebug()<<"3";
    }

    // get code
    void NetworkManager::SlotGetCode()
    {
        try
        {

            this->m_networkManager->deleteResource(this->m_request);

            if(this->m_reply->error() == QNetworkReply::NoError)
            {
                QList<QNetworkCookie>  codeCookie = qvariant_cast<QList<QNetworkCookie>>(m_reply->header(QNetworkRequest::SetCookieHeader));
                if (!codeCookie.isEmpty())
                    this->m_code.append(codeCookie[0].value());

                if (!this->m_code.isEmpty())
                    PostGrantType();
                else
                {
                    this->FisrtPageRequest();
                    return;
                }
            }
            else
            {
                const std::string str = m_reply->errorString().toStdString();
                throw std::exception(str.c_str());
            }
        }
        catch (const std::exception &ex)
        {
            QString error(ex.what());
            emit SignalGetErrorStr(error);
        }
qDebug()<<"4";
    }

    // get refresh_token
    void  NetworkManager::SlotPostGrantType(QNetworkReply*)
    {
        try
        {
            this->m_networkManager->deleteResource(this->m_request);
            if(this->m_reply->error() == QNetworkReply::NoError)
            {
                Parse::JSONParser parser;
                if (!m_reply->error())
                    this->m_replyStr = QString::fromUtf8(m_reply->readAll());

                QString token = parser.GetValue(this->m_replyStr, "refresh_token");

                PostRefreshToken(token);
            }
            else
            {
                const std::string str = m_reply->errorString().toStdString();
                throw std::exception(str.c_str());
            }
        }
        catch (const std::exception &ex)
        {
            QString error(ex.what());
            emit SignalGetErrorStr(error);
        }
    }

    void  NetworkManager::SlotPostRefreshToken(QNetworkReply*)
    {
        try
        {
            this->m_networkManager->deleteResource(this->m_request);

            if (this->m_reply->error() == QNetworkReply::NoError)
            {
                Parse::JSONParser parser;
                this->m_replyStr = QString::fromUtf8(this->m_reply->readAll());
                QString access_token = parser.GetValue(this->m_replyStr, "access_token");
                this->PostIssueToken(access_token);
            }
            else
            {
                const std::string str = m_reply->errorString().toStdString();
                throw std::exception(str.c_str());
            }
        }
        catch (const std::exception &ex)
        {
            QString error(ex.what());
            emit SignalGetErrorStr(error);
        }
    }



    void NetworkManager::SlotPostIssueToken(QNetworkReply*)
    {
        this->m_networkManager->deleteResource(this->m_request);

        Parse::JSONParser parser;

        QString strReply = QString::fromUtf8(this->m_reply->readAll());

        this->m_token = parser.GetValue(strReply, "token");

        if (!IsToken())
            emit GetToken();

        qDebug() << "Get toke";
        this->m_networkManager->blockSignals(true);
    }



    bool NetworkManager::IsToken()const throw()
    {
        return this->m_token.isEmpty();
    }

     QString NetworkManager::GetTok()const throw()
     {
         return this->m_token;
     }

}
