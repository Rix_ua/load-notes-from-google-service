#include "stdafx.h"
#include "treeitem.h"
#include "treemodel.h"

#include "Enums.h"

TreeModel::TreeModel(const QVector<Structs::Note>& m_notes, QObject *parent)
    : QAbstractItemModel(parent)
{
    QList<QVariant> rootData;
    rootData << "Notes" << "Created" << "Changed" << "Trashed" << "Deleted" << "isArchived";
    rootItem = new TreeItem(rootData);
    setupModelData(m_notes, rootItem);
}

TreeModel::~TreeModel()
{
    delete rootItem;
}

int TreeModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return static_cast<TreeItem*>(parent.internalPointer())->columnCount();
    else
        return rootItem->columnCount();
}

QVariant TreeModel::data(const QModelIndex &index, int role) const
{
    TreeItem *item = static_cast<TreeItem*>(index.internalPointer());
    int col = index.column();

    switch(role)
    {
    case Qt::DisplayRole:
//        if (col != 0 || item->childCount() > 0 )
        {
//            qDebug()<<"---In Display Role";
            return item->data(index.column());
        }
        break;
    case Qt::FontRole:
        if (col == 0 && (EImgLink == item->identifier() || EAnnImgLink == item->identifier() ) )
            //change font
        {
//            qDebug()<<"---In FontRole Role";
            QFont boldFont;
            boldFont.setBold(true);
            return boldFont;
        }
        break;
    }
    return QVariant();
}

Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return QAbstractItemModel::flags(index);
}

QVariant TreeModel::headerData(int section, Qt::Orientation orientation,
                               int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);

    return QVariant();
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent)
            const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    TreeItem *parentItem;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    TreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

QModelIndex TreeModel::parent(const QModelIndex &index) const
{
    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = static_cast<TreeItem*>(index.internalPointer());
    TreeItem *parentItem = childItem->parentItem();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

int TreeModel::rowCount(const QModelIndex &parent) const
{
    TreeItem *parentItem;
    if (parent.column() > 0)
        return 0;

    if (!parent.isValid())
        parentItem = rootItem;
    else
        parentItem = static_cast<TreeItem*>(parent.internalPointer());

    return parentItem->childCount();
}

void TreeModel::setupModelData(const QVector<Structs::Note>& m_notes, TreeItem *parent)
{
    QList<TreeItem*> parents;
    parents << parent;

    for (int numbOfNotes = 0; numbOfNotes < m_notes.size(); numbOfNotes++)
    {
        QList<QVariant> columnData;
        columnData << "Note " + QString::number(numbOfNotes+1) + "\t\t"<< m_notes[numbOfNotes].created
                   << m_notes[numbOfNotes].updated << m_notes[numbOfNotes].trashed
                   << m_notes[numbOfNotes].deleted << m_notes[numbOfNotes].isArchived ;
        parents.last()->appendChild(new TreeItem(columnData, parents.last()));

        if (!m_notes[numbOfNotes].LIST_Notes.isEmpty())
        {
            parents << parents.last()->child(parents.last()->childCount()-1);
            for (int listNotes = 0; listNotes < m_notes[numbOfNotes].LIST_Notes.size(); listNotes++ )
            {
                columnData.clear();
                columnData<<m_notes[numbOfNotes].LIST_Notes[listNotes];
                parents.last()->appendChild(new TreeItem(columnData, parents.last()));
            }
            parents.pop_back();
        }

        if (!m_notes[numbOfNotes].Images.isEmpty())
        {
            parents << parents.last()->child(parents.last()->childCount()-1);
            columnData.clear();
            columnData<<"media files";
            parents.last()->appendChild(new TreeItem(columnData, parents.last()));

            parents << parents.last()->child(parents.last()->childCount()-1);
            for (int listMedia = 0; listMedia < m_notes[numbOfNotes].Images.size(); listMedia++ )
            {
                columnData.clear();
                columnData<<m_notes[numbOfNotes].Images[listMedia].media_id;
                parents.last()->appendChild(new TreeItem(columnData, parents.last(), EImgLink));
            }
            parents.pop_back();
            parents.pop_back();
        }

        if (!m_notes[numbOfNotes].AnnNotes.isEmpty())
        {
            parents << parents.last()->child(parents.last()->childCount()-1);
            for (int listAnn = 0; listAnn < m_notes[numbOfNotes].AnnNotes.size(); listAnn++ )
            {
                columnData.clear();
                columnData<<"annotation"/* + QString::number(listAnn+1)*/;
                parents.last()->appendChild(new TreeItem(columnData, parents.last()));

                parents << parents.last()->child(parents.last()->childCount()-1);
                    columnData.clear();
                    columnData<<m_notes[numbOfNotes].AnnNotes[listAnn].title;
                    parents.last()->appendChild(new TreeItem(columnData, parents.last()));

                    columnData.clear();
                    columnData<<m_notes[numbOfNotes].AnnNotes[listAnn].Url;
                    parents.last()->appendChild(new TreeItem(columnData, parents.last()));

                    columnData.clear();
                    columnData<<m_notes[numbOfNotes].AnnNotes[listAnn].imageUrl;
                    parents.last()->appendChild(new TreeItem(columnData, parents.last(), EAnnImgLink));

                    columnData.clear();
                    columnData<<m_notes[numbOfNotes].AnnNotes[listAnn].provenanceUrl;
                    parents.last()->appendChild(new TreeItem(columnData, parents.last()));
                parents.pop_back();
            }
            parents.pop_back();
        }
    }
}
