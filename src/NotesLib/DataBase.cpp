#include "stdafx.h"
#include "DataBase.h"


DataBase::DataBase(const QString& login)
{
    QString path;

    this->GetPath(login, path);

    QString connection = QString(QSqlDatabase::defaultConnection);

    if (QSqlDatabase::contains(connection))
    {
        m_db = QSqlDatabase::database(connection);
    }
    else
    {
        // we are connected through QSQLITE the driver
        m_db = QSqlDatabase::addDatabase("QSQLITE", connection);
        // we enter name DB
        m_db.setDatabaseName(path);
        // we open connection
        if(!m_db.open())
            qDebug() << m_db.lastError();
    }
}


DataBase::~DataBase()
{
    if(m_db.isOpen())
            m_db.close();
}

QSqlError DataBase::InsetNote(const QVector<Structs::Note>& notes)
{
    QSqlQuery query(m_db);

    /**
    Create tables for a database
    */
    if (!query.exec("create table if not exists Notes(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, "
                                  "NOTE TEXT, parentId TEXT, created TEXT, updated TEXT, trashed TEXT, deleted TEXT, isArchived TEXT)"))
        return query.lastError();

    if (!query.exec("create table if not exists Image(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, media_id TEXT, parentId TEXT, type TEXT)"))
        return query.lastError();

    if (!query.exec("create table if not exists Annotations(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, url TEXT, title TEXT, imageUrl TEXT, provenanceUrl TEXT, parentId TEXT)"))
        return query.lastError();
    // Loading the data into the database
    for (int i = 0; i < notes.size(); ++i)
    {
        // If notes, load the database
        if (!notes.at(i).LIST_Notes.isEmpty())
        {
            if (!query.prepare("insert into Notes(NOTE, parentId, created, updated, trashed, deleted, isArchived) values(?, ?, ?, ?, ?, ?, ?)"))
                return query.lastError();
            this->AddNote(query, notes.at(i).LIST_Notes, notes.at(i).parentId, notes.at(i).created, notes.at(i).updated, notes.at(i).trashed, notes.at(i).deleted, notes.at(i).isArchived);
        }
        // If images, load the database
        if (!notes.at(i).Images.isEmpty())
        {
            if (!query.prepare("insert into Image(media_id, parentId, type) values(?, ?, ?)"))
                return query.lastError();
            this->AddImage(query, notes.at(i).Images, notes.at(i).parentId);
        }
        // If annotations, load
        if (!notes.at(i).AnnNotes.isEmpty())
        {
            if (!query.prepare("insert into Annotations(url, title, imageUrl, provenanceUrl, parentId) values(?, ?, ?, ?, ?)"))
                return query.lastError();
            this->AddAnnotations(query, notes.at(i).AnnNotes, notes.at(i).parentId);
        }
    }


    return QSqlError();

}
// add Note
void DataBase::AddNote(QSqlQuery& q, const QVector<QString>& Notes, const QString& parentId,
                       const QString& create, const QString& update, const QString& trashed, const QString& deleted, const bool& isArch)
{
    for (int i = 0; i < Notes.size(); ++i)
    {
        q.addBindValue(Notes.at(i));
        q.addBindValue(parentId);
        q.addBindValue(create);
        q.addBindValue(update);
        q.addBindValue(trashed);
        q.addBindValue(deleted);
        q.addBindValue(isArch);
        q.exec();
    }
}
// add Image_Id
void DataBase::AddImage(QSqlQuery& q, const QVector<Structs::Media>& Image, const QString& parentId)
{
    for (int i = 0; i < Image.size(); ++i)
    {
        q.addBindValue(Image.at(i).media_id);
        q.addBindValue(parentId);
        q.addBindValue(Image.at(i).type);
        q.exec();
    }
}

// add Annotations
void DataBase::AddAnnotations(QSqlQuery& q, const QVector<Structs::Annotations>& Image, const QString& parentId)
{
    for (int i = 0; i < Image.size(); ++i)
    {
        q.addBindValue(Image.at(i).Url);
        q.addBindValue(Image.at(i).title);
        q.addBindValue(Image.at(i).imageUrl);
        q.addBindValue(Image.at(i).provenanceUrl);
        q.addBindValue(parentId);
        q.exec();
    }
}
// Create Directory DB, with file .sqlite
void DataBase::GetPath(const QString& login, QString& path)
{
    QDir dir;
    QTextStream writStr(&path);
    writStr << QCoreApplication::applicationDirPath() << "/DB/";

    // Creation of a directory for storage of the database
    if(!dir.mkdir(path))
        qDebug() << "error create dir11";

    writStr << login << ".sqlite";
}
