#pragma once
#include "stdafx.h"


namespace NetworkConst
{
    // Constants for inquiries
    const QUrl FirstUrl("https://accounts.google.com/ServiceLogin?service=memento&passive=1209600");
    const QUrl SecondUrl("https://accounts.google.com/accountLoginInfoXhr");
    const QUrl ThirdUrl("https://accounts.google.com/signin/challenge/sl/password");
    const QUrl RequestLocation("https://accounts.google.com/ServiceLogin?service=memento&passive=1209600");
    const QUrl UrlGetCode("https://accounts.google.com/o/oauth2/programmatic_auth?scope=https://www.google.com/accounts/OAuthLogin&client_id=77185425430.apps.googleusercontent.com&device_type=chrome&authuser=0");

    // Constants on receiving a token, and notes
    const QUrl Oauth2V4("https://www.googleapis.com/oauth2/v4/token");
    const QUrl IssueToken("https://www.googleapis.com/oauth2/v2/IssueToken");
    const QUrl GetNotes("https://www.googleapis.com/notes/v1/changes/?key=AIzaSyDzSyl-DPNxSyc7eghRsB4oNNetrnvnH0I");

    const QString SCOPE("https://www.google.com/accounts/OAuthLogin");
    const QString GRANT_TYPE("authorization_code");
    const QString CLIENT_ID("77185425430.apps.googleusercontent.com");
    const QString CLIENT_SECRET("OTJgUOQcT7lO7GsGZq2G4IlT");

    const QString SCOPE_ISSUE("https://www.googleapis.com/auth/cclog https://www.googleapis.com/auth/client_channel https://www.googleapis.com/auth/drive https://www.googleapis.com/auth/memento https://www.googleapis.com/auth/offers.lmp https://www.googleapis.com/auth/plus.peopleapi.readwrite https://www.googleapis.com/auth/reminders");
    const QString CLIEND_ID_ISSUE("192748556389-u13aelnnjsmn5df1voa2d3oimlbd8led.apps.googleusercontent.com");
    const QString ORIGIN("hmjkmjkepdijhoojdojkdfohbdgmmhki");
    const QString LIV_VER("extension");
    const QByteArray GetNotes_ORIGIN("chrome-extension://hmjkmjkepdijhoojdojkdfohbdgmmhki");

    const QByteArray LAST_REQ("{\"requestHeader\":{\"clientVersion\":{\"major\":\"3\",\"minor\":\"2\",\"build\":\"7\",\"revision\":\"0\"},\"clientPlatform\":\"CRX\",\"capabilities\":[{\"type\":\"TR\"},{\"type\":\"EC\"},{\"type\":\"SH\"},{\"type\":\"LB\"},{\"type\":\"RB\"},{\"type\":\"AN\"},{\"type\":\"EX\"},{\"type\":\"PI\"}],\"clientLocale\":\"uk\"},\"targetVersion\":\"\",\"nodes\":[]}");

    // For downloading of pictures
    const QString GET_LINK_FOR_IMAG("?accept=image/gif,image/jpeg,image/jpg,image/png,image/webp,audio/aac&sz=600&key=AIzaSyDzSyl-DPNxSyc7eghRsB4oNNetrnvnH0I");
}
