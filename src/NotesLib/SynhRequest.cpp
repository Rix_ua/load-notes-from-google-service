#include "SynhRequest.h"



SynhRequest::SynhRequest()
{
}


SynhRequest::~SynhRequest()
{
}


void SynhRequest::Wait(int Time_Ms)
{
	std::unique_lock<std::mutex> lk(Synk_Req);
	auto timePoint = std::chrono::system_clock::now();
	cond_var.wait_until(lk, timePoint + std::chrono::milliseconds(Time_Ms));
}