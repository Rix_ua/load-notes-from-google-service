#include "stdafx.h"
#include "SettingManager.h"
namespace WorkingInNetwork
{


    SettingManager::SettingManager()
    {
    }


    SettingManager::~SettingManager()
    {
    }


    // Заполнение на первый запрос
    void SettingManager::WriteRequestFirst(QNetworkRequest& request)
    {
        request.setUrl(NetworkConst::FirstUrl);
        this->WriteHeadRequest(request);

    }
    // Заполнение на второй запрос
    void SettingManager::WriteRequestSecond(QNetworkRequest &req, const QString& m_mail,
        Structs::RequestField&ReqParams, QByteArray& postData)
    {
        req.setUrl(NetworkConst::SecondUrl);
        req.setRawHeader("Content-type", "application/x-www-form-urlencoded");

        postData.append("Email=" + m_mail);
        postData.append("&requestlocation=" + NetworkConst::RequestLocation.toString());
        postData.append("&Page=PasswordSeparationSignIn");
        postData.append("&GALX=" + ReqParams.GALX);
        postData.append("&gxf=" + ReqParams.gxf);
        postData.append("&service=memento");
        postData.append("&ltmpl=keep");
    }
    // Заполнение третего запроса на получение - code
    void SettingManager::WriteRequestThrid(QNetworkRequest & req, const QString& pass,const QString& mail,
        Structs::RequestField &ReqParams, QByteArray& postData)
    {
        req.setUrl(NetworkConst::ThirdUrl);
        this->WriteHeadRequest(req);
        req.setRawHeader("Content-type", "application/x-www-form-urlencoded");


        postData.clear();
        postData.append("Page=PasswordSeparationSignIn&");
        postData.append("GALX=" + ReqParams.GALX);
        postData.append("&gxf=" + ReqParams.gxf);
        postData.append("&ProfileInformation=" + ReqParams.ProfileInformation);
        postData.append("&Email=" + mail);
        postData.append("&Passwd=" + pass);
    }
    // Заполнение четвертого запроса
    void SettingManager::WriteRequestGetCode(QNetworkRequest& req)
    {
        req.setUrl(NetworkConst::UrlGetCode);
        this->WriteHeadRequest(req);
    }
    // Заполнение запроса на получение grantType, refresh_Token
    void SettingManager::WriteRequestGrantType(QNetworkRequest&req, const QString& code, QByteArray& postData)
    {
        req.setUrl(NetworkConst::Oauth2V4);

        this->WriteRawHeader(req);

        postData.clear();
        postData.append("scope=" + NetworkConst::SCOPE);
        postData.append("&grant_type=" + NetworkConst::GRANT_TYPE);
        postData.append("&client_id=" + NetworkConst::CLIENT_ID);
        postData.append("&client_secret=" + NetworkConst::CLIENT_SECRET);
        postData.append("&code=" + code);
    }
    // этот запрос на получение access_token
    void SettingManager::WriteRequestRefreshToke(QNetworkRequest&req, const QString& Ref_Token, QByteArray&postData)
    {
        req.setUrl(NetworkConst::Oauth2V4);
        this->WriteRawHeader(req);

        postData.clear();
        postData.append("client_id=" + NetworkConst::CLIENT_ID);
        postData.append("&client_secret=" + NetworkConst::CLIENT_SECRET);
        postData.append("&grant_type=refresh_token");
        postData.append("&refresh_token=" + Ref_Token);
    }
    // метод для заполнения запроса на получение токена
    void SettingManager::WriteRequestIssueToken(QNetworkRequest&req, const QString& token,
        QByteArray& postData, QByteArray& arr)
    {
        postData.clear();
        postData.append("Bearer " + token);

        req.setUrl(NetworkConst::IssueToken);

        this->WriteRawHeader(req);
        req.setRawHeader("Authorization", postData);

        arr.append("force=false");
        arr.append("&response_type=token");
        arr.append("&scope=" + NetworkConst::SCOPE_ISSUE);
        arr.append("&client_id=" + NetworkConst::CLIEND_ID_ISSUE);
        arr.append("&origin=" + NetworkConst::ORIGIN);
        arr.append("&lib_ver=" + NetworkConst::LIV_VER);
    }


    // метод для заполнения заголовка авторизации на сайте google.com
    void SettingManager::WriteHeadRequest(QNetworkRequest& request)
    {
        request.setRawHeader("Host", "accounts.google.com");
        request.setRawHeader("User-Agent", "Mozilla / 5.0 (Windows NT 6.1; WOW64) AppleWebKit / 537.36 (KHTML, like Gecko) Chrome / 53.0.2785.143 Safari / 537.36 OPR / 40.0.0.230818004 (Edition Campaign 34 EXP23)");
    }
    // метод для заполнения заголовка для OAuth протокола
    void SettingManager::WriteRawHeader(QNetworkRequest&req)
    {
        req.setRawHeader("Host", "www.googleapis.com");
        req.setRawHeader("Content-Type", "application/x-www-form-urlencoded");
    }
    //  метод на получение заметок
    void SettingManager::WriteRequestGetNotes(QNetworkRequest&request, QByteArray& arr, const QString & token)
    {
        arr.clear();
        arr.append("Bearer " + token);

        request.setUrl(NetworkConst::GetNotes);
        request.setRawHeader("Host", "www.googleapis.com");
        request.setRawHeader("Content-Type", "application/json");
        request.setRawHeader("User-Agent", " Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36");
        request.setRawHeader("Origin", NetworkConst::GetNotes_ORIGIN);
        request.setRawHeader("Authorization", arr);
    }
    // метод для скачивания картинок
    void SettingManager::WriteRequestGetLinkForImage(QNetworkRequest& req, const QString& media_Id, const QByteArray& bearer)
    {
        QString strLink;
        QTextStream out(&strLink);
        out << "https://keep.google.com/media/" << media_Id << NetworkConst::GET_LINK_FOR_IMAG;

        req.setUrl(static_cast<const QUrl>(strLink));


        req.setRawHeader("Host", "keep.google.com");
        WriteHeadLinkRequest(req, bearer);
    }

    void SettingManager::WriteRequestForImage(QNetworkRequest& req, const QByteArray& bearer, const QString &linkHost)
    {
        QByteArray arr;
        arr.append(linkHost);
        req.setRawHeader("Host",arr);
        WriteHeadLinkRequest(req,bearer);

    }

    void SettingManager::WriteHeadLinkRequest(QNetworkRequest& req, const QByteArray& bearer)
    {
        req.setRawHeader("User-Agent", "Mozilla / 5.0 (Windows NT 6.1; WOW64) AppleWebKit / 537.36 (KHTML, like Gecko) Chrome / 53.0.2785.143 Safari / 537.36 OPR / 40.0.0.230818004 (Edition Campaign 34 EXP23)");
        req.setRawHeader("Authorization", bearer);
        req.setRawHeader("Accept", "image/webp,image/*,*/*;q=0.8");
    }
}
