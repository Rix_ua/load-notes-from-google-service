CONFIG += console network c++14

INCLUDEPATH += "gtests_src/include"
INCLUDEPATH += "gtests_src"
INCLUDEPATH += "../"

SOURCES += \
    mainTESTS.cpp \
    gtests_src/src/gtest.cc \
    gtests_src/src/gtest-all.cc \
    gtests_src/src/gtest-death-test.cc \
    gtests_src/src/gtest-filepath.cc \
    gtests_src/src/gtest-port.cc \
    gtests_src/src/gtest-printers.cc \
    gtests_src/src/gtest-test-part.cc \
    gtests_src/src/gtest-typed-test.cc \
    ../NotesApp/JSONParser.cpp

HEADERS += \
    ../NotesApp/JSONParser.h \
    ../NotesApp/Structs.h

RESOURCES += \
    datafortests.qrc

