#include "gtest/gtest.h"

#include <QFile>
#include <QJsonParseError>
#include "JSONParser.h"
namespace Parse
{
class JSONParser;
}
class TestJSonParser : public ::testing::Test
{
protected:
    TestJSonParser ()
    {
        filePath0 = ":/json_input_files/json0.txt";
        filePath1 = ":/json_input_files/json1.txt";
        filePath2 = ":/json_input_files/json2.txt";
    }
    void SetUp(const QString & filePath)
    {
        getJsonObjFromFile (filePath, inJsonObj);
        jsParser.ParsJson(inJsonObj, outVect);
    }
//    input data for the function test - QJsonObject
//    so we read the "JSON" files and transform them into the QJsonObject
    void getJsonObjFromFile (const QString & filePath, QJsonObject & jObj)
    {
        QFile f(filePath);
        f.open(QFile::ReadOnly);
        QJsonDocument jDoc = QJsonDocument::fromJson(f.readAll());
        f.close();
        jObj = jDoc.object();
    }
    QJsonObject inJsonObj;
    QVector<Structs::Note> outVect;
    Parse::JSONParser jsParser;
    QString filePath0, filePath1, filePath2;
};

TEST_F (TestJSonParser, file_0_sizeInOutData)
{//Json file without notes
    SetUp(filePath0);

    ASSERT_EQ(false, inJsonObj.empty());
    ASSERT_EQ(true, outVect.empty());
}

TEST_F (TestJSonParser, file_1_sizeInOutData)
{
    SetUp(filePath1);

    ASSERT_EQ(false, inJsonObj.empty());
    ASSERT_EQ(false, outVect.empty());
    EXPECT_EQ(12, outVect.size());
}

TEST_F (TestJSonParser, file_1_valuesOfOutVector)
{
    SetUp(filePath1);

    ASSERT_EQ(true, outVect[0].Images.empty());
    ASSERT_EQ(1, outVect[0].LIST_Notes.size());
    EXPECT_STREQ("ееее\n", outVect[0].LIST_Notes[0].toStdString().c_str());

    ASSERT_EQ(1, outVect[1].LIST_Notes.size());
    EXPECT_STREQ("Тест Картинок\n", outVect[1].LIST_Notes[0].toStdString().c_str());
    ASSERT_EQ(8, outVect[1].Images.size());
    EXPECT_STREQ("1DI1u-mlrSlHVsCt6xOwhF6g5VI_TEm0",
                 outVect[1].Images[0].media_id.toStdString().c_str());
    EXPECT_STREQ("1z-NCS_GYS-uXlJkLhKIIuu4_LAlRYqU",
                 outVect[1].Images[7].media_id.toStdString().c_str());
}

TEST_F (TestJSonParser, file_2_sizeInOutData)
{
    SetUp(filePath2);

    ASSERT_EQ(false, inJsonObj.empty());
    ASSERT_EQ(false, outVect.empty());
    EXPECT_EQ(4, outVect.size());
}

TEST_F (TestJSonParser, file_2_valuesOfOutVector)
{
    SetUp(filePath2);

    ASSERT_EQ(true, outVect[0].Images.empty());
    ASSERT_EQ(1, outVect[0].LIST_Notes.size());
    EXPECT_STREQ("Курсы С++", outVect[0].LIST_Notes[0].toStdString().c_str());

    ASSERT_EQ(true, outVect[2].Images.empty());
    ASSERT_EQ(4, outVect[2].LIST_Notes.size());
    EXPECT_STREQ("HW Multithreading", outVect[2].LIST_Notes[1].toStdString().c_str());
    EXPECT_STREQ("SQLite or QtSql", outVect[2].LIST_Notes[3].toStdString().c_str());
}

int main(int argc, char* argv[])
{
    ::testing::InitGoogleTest(&argc, argv);

    RUN_ALL_TESTS();
    system("pause");
    return 0;
}
